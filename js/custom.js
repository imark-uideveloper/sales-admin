(function($) {
  $(window).on("load", function() {
    $(".table-cover").mCustomScrollbar({
      axis: "x"
    });

  });
})(jQuery);


(function($) {
  $(window).on("load", function() {
    $(".scroll").mCustomScrollbar({
      axis: "Y" // horizontal scrollbar
    });
  });
})(jQuery);




wow = new WOW({
  mobile: false, // default
})
wow.init();


//document.getElementById("time").innerHTML = Date(h);
var currentdate = new Date();
var dateTime = currentdate.getHours() + ":" +
  currentdate.getMinutes() + ":" +
  currentdate.getSeconds();

$('#time').text(dateTime);

$(function() {
    //----- OPEN
    $('[pd-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('pd-popup-open');
        $('[pd-popup="' + targeted_popup_class + '"]').fadeIn(100);
        $("body").addClass("popupOpen");

        e.preventDefault();
    });

    //----- CLOSE
    $('[pd-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('pd-popup-close');
        $('[pd-popup="' + targeted_popup_class + '"]').fadeOut(200);
        $("body").removeClass("popupOpen");
        e.preventDefault();
    });
});
